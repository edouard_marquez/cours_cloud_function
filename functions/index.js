/**
 * Commandes :
 * npm install
 * npm install -g firebase-tools
 * firebase login
 * firebase use --add
 *
 * Pour tester en local : firebase emulators:start
 */
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const { TopicBuilder } = require('firebase-functions/lib/providers/pubsub');
admin.initializeApp(functions.config().firebase);

// Requête à faire sur /sendFCMMessage
exports.sendFCMMessage = functions.https.onRequest((request, response) => {
    console.log('Hello World!');

    // TODO


    response.status(200).send()
  }
);

function sendFCMMessage(token) {
  var payload = {
    notification: {
      title: '`$FooCorp` up 1.43% on the day',
      body: 'FooCorp gained 11.80 points to close at 835.67, up 1.43% on the day.'
    }
  }

  return admin.messaging().sendToDevice(registrationToken = token, payload = payload);
}
